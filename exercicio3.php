<?php
/**
 * Escreva uma função em PHP para pegar determinar a extensão dos 3
 * arquivos abaixo e mostrar as extensões na tela. As saídas devem ser
 * mostradas em uma lista em ordem alfabética.
 * 
 * Arquivos de exemplo
 *  a) music.mp4
 *  b) video.mov
 *  c) imagem.jpeg
 * 
 * Saida experada:
 *  1 .jpeg
 *  2 .mov
 *  3 .mp4 
 */

 $files = array (
    'music.mp4',
    'video.mov',
    'imagem.jpeg'
 );

 function _extractExtension( $file_list = null ) {

    $extensions = array();

    if ( !is_null($file_list) ) {
        foreach ($file_list as $file) {
            $extension = explode('.',$file);
            if ( !in_array($extension['1']) ) {
                $extensions[$extension['1']] = '.'.$extension['1'];
            }
        }
        
        if ( !empty($extensions) ) {
            ksort($extensions);
            echo '<ol>';
                foreach ($extensions as $extension) {
                    echo '<li>'. $extension .'</li>';
                }
            echo '</ol>';
        }
        
    } else {
        echo 'LISTA DE ARQUIVOS VAZIA';
    }
 }

_extractExtension($files);