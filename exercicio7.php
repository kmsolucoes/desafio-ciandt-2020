<?php
/**
 * Crie uma API simples para manipular uma lista de usuários contendo os
 * campos (Nome, Sobrenome, Email & Telefone.). Essa API deverá conter
 * os requisitos abaixo:
 *  a. Dados deverão ser salvos em um arquivo de texto
 *  b. Usar Rest API
 *  c. Criar Endpoint para listar todos os usuarios
 *  d. Criar Endpoint para deletar usuarios por email
 *  e. Criar Endpoint para adicionar um usuario novo
 *  f. Criar Endpoint para atualizar os dados do usuario.
 *  g. Prover documentacao minima para usar a API.
 */

 echo "<b>Exercício 7: </b>";
 echo "Crie uma API simples para manipular uma lista de usuários contendo os
 campos (Nome, Sobrenome, Email & Telefone.). Essa API deverá conter
 os requisitos abaixo:<br>
 <b>a.</b> Dados deverão ser salvos em um arquivo de texto<br />";
 echo "<i>Todos os dados serão salvo em /files/txt/api_users.txt</i><br /><br />";
 echo "<b>b.</b> Usar Rest API<br /><br />";
 echo "<b>c.</b> Criar Endpoint para listar todos os usuarios<br />";
 echo "<i>Endpoint disponível em: <link>/api/users/read.php</i><br /><br />";
 echo "<b>d.</b> Criar Endpoint para deletar usuarios por email<br />";
 echo "<i>Endpoint disponível em: <link>/api/users/delete.php</i><br />";
 echo "<i>Os parâmetros deverão ser enviados da seguinte forma:</i>";
 echo "<ul>";
 echo "<li><i>Body / Raw</i></li>";
 echo "<li><i>O valor deverá ser passado em formato JSON. Ex.: {\"mail\":\"karim.maluf@gmail.com\"}</i></li>";
 echo "</ul><br />";
 echo "<b>e.</b> Criar Endpoint para adicionar um usuario novo<br />";
 echo "<i>Endpoint disponível em: <link>/api/users/create.php</i><br />";
 echo "<i>Os parâmetros deverão ser enviados da seguinte forma:</i>";
 echo "<ul>";
 echo "<li><i>Body / Raw</i></li>";
 echo "<li><i>Os valores deverão ser passados em formato JSON. Ex.: <br />
       {<br />
            \"firstName\":\"Karim \",<br />
            \"lastName\":\"Maluf\",<br />
            \"phone\":\"19741258963\"<br />
            \"mail\":\"mail@example.com\"<br />
        }</i></li>";
 echo "<li><i><b>Observação: </b> Os parâmetros 'firstName' e 'mail' são obrigatórios!</i></li>";
 echo "</ul><br /><br />";
 echo "<b>f.</b> Criar Endpoint para atualizar os dados do usuario.<br />";
 echo "<i>Endpoint disponível em: <link>/api/users/update.php</i><br />";
 echo "<i>Os parâmetros deverão ser enviados da seguinte forma:</i>";
 echo "<ul>";
 echo "<li><i>Body / Raw</i></li>";
 echo "<li><i>Os valores deverão ser passados em formato JSON. Ex.: <br />
       {<br />
            \"firstName\":\"Karim \",<br />
            \"lastName\":\"Maluf\",<br />
            \"phone\":\"19741258963\"<br />
            \"mail\":\"mail@example.com\"<br />
        }</i></li>";
 echo "<li><i><b>Observação: </b> Os parâmetros 'firstName' e 'mail' são obrigatórios!</i></li>";        
 echo "</ul><br /><br />";