<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$content = array();
if (file_exists('../../files/txt/api_users.txt')) {
    $file = file_get_contents('../../files/txt/api_users.txt');
    $content[] = unserialize($file);

    // set response code - 200 OK
    http_response_code(200);

    // show users data in json format
    echo json_encode($content);
} else {
    // set response code - 400 Not Found
    http_response_code(400);

    // show no results found
    echo json_encode(
        array("message" => "No users found.")
    );
}