<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if (file_exists('../../files/txt/api_users.txt')) {
    $file = file_get_contents('../../files/txt/api_users.txt');
    $content = unserialize($file);
    
    $found = false;
    $result = array();

    // get user email
    $data = json_decode(file_get_contents("php://input"));

    foreach ($content as $register) {
        if ( in_array($data->mail, $register) ) {
            $found = true;
        } else {
            $result[] = $register;
        }
    }
    
    if ( $found ) {
        file_put_contents('../../files/txt/api_users.txt', serialize($result));
        
        http_response_code(200); //OK
        echo json_encode(array("message" => "Usuário removido com sucesso."));
    } else {
        http_response_code(404); //NOT FOUND THE USER
        echo json_encode(array("message" => "Usuário nao encontrato."));
    }

} else {
    http_response_code(503); //FILE NOT FOUND
    echo json_encode(array("message" => "Problema ao remover usuário."));
}