<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if (file_exists('../../files/txt/api_users.txt')) {
    $file = file_get_contents('../../files/txt/api_users.txt');
    $content = unserialize($file);
    //$result = array();
    
    // get user information
    $data = json_decode(file_get_contents("php://input"));

    if ( empty($data->firstName) ) {
        http_response_code(404); //NAME CANNOT BE EMPTY
        echo json_encode(array("message" => "Nome não informado."));
    } else if ( empty($data->mail) ) {
        http_response_code(404); //MAIL CANNOT BE EMPTY
        echo json_encode(array("message" => "E-mail não informado."));
    } else {
        foreach ($content as $register) {
            if ( in_array($data->mail, $register) ) {
                
                $register['firstName']  = $data->firstName;
                $register['lastName']   = $data->lastName;
                $register['phone']      = $data->phone;
                $register['mail']       = $data->mail;
                
                $result[] = $register;
            } else if ( !empty($register) ) {
                $result[] = $register;
            }
        }

        file_put_contents('../../files/txt/api_users.txt', serialize($result));
        
        http_response_code(200); //OK
        echo json_encode(array("message" => "Usuário atualizado com sucesso."));
    }

} else { 
    http_response_code(404); //OK
    echo json_encode(array("message" => "Não existem registros para serem atualizados."));
}