<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get user information
$data = json_decode(file_get_contents("php://input"));

if (file_exists('../../files/txt/api_users.txt')) {
    $file = file_get_contents('../../files/txt/api_users.txt');
    $content = unserialize($file);
}

if ( empty($data->firstName) ) {
    http_response_code(404); //NAME CANNOT BE EMPTY
    echo json_encode(array("message" => "Nome não informado."));
} else if ( empty($data->mail) ) {
    http_response_code(404); //MAIL CANNOT BE EMPTY
    echo json_encode(array("message" => "E-mail não informado."));
} else {
    $user = array(
        'firstName' => $data->firstName,
        'lastName'  => $data->lastName,
        'mail'      => $data->mail,
        'phone'     => $data->phone,
    );

    $content[] = $user;
    file_put_contents('../../files/txt/api_users.txt', serialize($content));
        
    http_response_code(200); //OK
    echo json_encode(array("message" => "Usuário inserido com sucesso."));
}

