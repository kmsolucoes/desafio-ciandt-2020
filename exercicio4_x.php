<?php

//b
function _checkAndSaveInformation( $info ) {
    $to_save = true;
    $reason  = '';

    if (file_exists('files/txt/registros.txt')) {
        $file = file_get_contents('files/txt/registros.txt');
        $content[] = unserialize($file);

        foreach ($content as $register) {
            //c
            if ( in_array($info['email'], $register) ) {
                $to_save = false;
                $reason = 'E-mail';
            } else if ( in_array($info['login'], $register) ) {
                $to_save = false;
                $reason = 'Login';
            }
        }

        if ($to_save) {
            $content[] = $form_values;
            file_put_contents('files/txt/registros.txt', serialize($content), FILE_APPEND);
            echo 'Registro armazenado com sucesso';
        } else {
            echo $reason . ' já cadastrado!';
        } 
    } else {
        file_put_contents('files/txt/registros.txt', serialize($info), LOCK_EX);
    }
}

$form_values = array(
    'nome'      => $_POST['firstName'],
    'sobrenome' => $_POST['lastName'],
    'email'     => $_POST['userEmail'],
    'telefone'  => $_POST['userPhone'],
    'login'     => $_POST['userLogin'],
    'senha'     => $_POST['userPass'],
);

_checkAndSaveInformation($form_values);
