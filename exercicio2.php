<?php
/**
 * Joãozinho vai morder o seu dedo 50% das vezes. Esse exercício será
 * dividido em 2 partes.
 * 
 *  a) Primeiro, cria uma função chamada foiMordido() que deverá retornar TRUE para 50%
 *  das vezes e FALSE para os outros 50%. A função rand() pode ser útil aqui.
 * 
 *  b) Após criar a função, crie um código/página que mostre as frases “Joãozinho mordeu o
 *  seu dedo !” ou “Joaozinho NAO mordeu o seu dedo !” usando a função foiMordido() que
 *  foi criado na primeira parte.
 */

 echo "<b>Exercício 2: </b>";
 echo "Joãozinho vai morder o seu dedo 50% das vezes. Esse exercício será dividido em 
 2 partes.<br/>";
 echo "<b>a)</b> Primeiro, cria uma função chamada foiMordido() que deverá retornar TRUE 
 para 50% das vezes e FALSE para os outros 50%. A função rand() pode ser útil aqui.<br />";
 echo "b) Após criar a função, crie um código/página que mostre as frases “Joãozinho 
 mordeu o seu dedo !” ou “Joaozinho NAO mordeu o seu dedo !” usando a função foiMordido() 
 que foi criado na primeira parte.";
 echo "<br /><br />";


 class pessoa {

    private $name;
    private $foiMordido;

    function __construct ( $name = null ) {
        $this->name = $name;
        $this->foiMordido = false;
    }

    public function setMordida ( $mordida ) {
        $this->foiMordido = $mordida;
    }

    public function setName ( $name ) {
        $this->name = $name;
    }

    public function getMordida() {
        return $this->foiMordido;
    }

    public function getName() {
        return $this->name;
    }

    //a
    public function foiMordido( $qtd ) {

        for ( $i = 0; $i < $qtd; $i++ ) {
            if ( $this->foiMordido === true ) {
                if ( !is_null($this->name) ) {
                    echo $this->name . ' mordeu o seu dedo! <br />';
                } else {
                    echo $this->foiMordido. '<br />';
                }
                $this->foiMordido = false;
            } else if ( $this->foiMordido === false ) {
                if ( !is_null($this->name) ) {
                    echo $this->name . ' NAO mordeu o seu dedo! <br />';
                } else {
                    echo $this->foiMordido . '<br />';
                }
                $this->foiMordido = true;
            }
        }
    }
 }

 //b
 $pessoa = new pessoa('Joaozinho');
 $pessoa->foiMordido(10);
