<?php
/**
 * Crie uma Classe para criar um select Field para um user registration form.
 * Após criar essa classe crie um webform simples e adicione esse campo
 * criado.
 */

 echo "<b>Exercício 6: </b>";
 echo "Crie uma Classe para criar um select Field para um user registration form. Após 
 criar essa classe crie um webform simples e adicione esse campo criado.";
 echo "<br /><br />";

class selectField {
    
    private $name;
    private $options = array();
    private $selectField;

    function __construct($label, $select_name, $options = null) {
        $this->name     = $select_name;
        $this->options  = $options;

        $this->selectField  = "<label for='". $this->name . "'>". $label . " : </label>";
        $this->selectField .= "<select id='" . $this->name . "'>";

        if ( !is_null($this->options) && !empty($this->options) ) {    
            foreach ($this->options as $key => $value) {
                $this->selectField .="<option value='" . $key . "'>" . $value . "</option>";
            }
        } else {
            $this->selectField .="<option value='none'>Selecione um valor</option>";
        }

        $this->selectField .= "</select>";
    }

    public function getSelectField() {
        return $this->selectField;
    }
    
}

$valuesGender = array(
    'masculino' => 'Masculino',
    'feminino'  => 'Feminino',
);

$selectGender = new selectField('Sexo', 'userGender', $valuesGender);

?>
<html>
    <body>
        <form name='userForm' id='userForm'>
            <div class='field'>
                <label for='firstName'>Nome:</label>
                <input type='text' name='firstName' id='firstName' value='' />
            </div>

            <div class='field'>
                <label for='lastName'>Sobrenome:</label>
                <input type='text' name='lastName' id='lastName' value='' />
            </div>

            <div class='field'>
                <label for='userEmail'>Email:</label>
                <input type='email' name='userEmail' id='userEmail' value='' />
            </div>

            <div class='field'>
                <label for='userPhone'>Telefone:</label>
                <input type='number' name='userPhone' id='userPhone' value='' />
            </div>

            <div class='field'>
                <label for='userLogin'>Login:</label>
                <input type='text' name='userLogin' id='userLogin' value='' />
            </div>

            <div class='field'>
                <label for='userPass'>Senha:</label>
                <input type='password' name='userPass' id='userPass' value='' />
            </div>

            <div class='field'>
                <?php print $selectGender->getSelectField(); ?>
            </div>

            <div class='button'>
                <button name="submit" onClick="sendInformation();">Cadastrar!</button>
            </div>
        </form>
    </body>
</html>
