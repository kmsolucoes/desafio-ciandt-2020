<?php
/**
 * Crie um parser que converte um arquivo XML em um arquivo CSV usando
 * PHP.
 */

 echo "<b>Exercício 5: </b>";
 echo "Crie um parser que converte um arquivo XML em um arquivo CSV usando PHP.";
 echo "<br /><br />";

 $xmlPath       = 'files/xml/';
 $csvPath       = 'files/csv/';
 $xmlFileName   = 'cd_catalog.xml';
 $header_set    = false;

 $fullXmlPath   = $xmlPath . $xmlFileName;
 if ( file_exists($fullXmlPath) ) {
    $xml = simplexml_load_file($fullXmlPath);
    $name = explode('.',$xmlFileName);

    $fullCsvPath = $csvPath . $name['0'].'.csv';  
    $handle      = fopen( $fullCsvPath, 'w+');
    
    foreach ($xml as $key => $value) {
        if(!$header_set) {
			fputcsv($handle, array_keys(get_object_vars($value)));
			$header_set = true;
		}
		fputcsv($handle, get_object_vars($value));
    }

    fclose($handle);
    echo 'ARQUIVO CONVERTIDO COM SUCESSO';
 } else {
     echo 'ARQUIVO NÃO ENCONTRADO';
 }
