<?php
/**
 * Crie um formulário contendo os campos (Nome, Sobrenome, e-mail,
 * telefone, login & senha) e salve as submissões dentro de um arquivo
 * chamado registros.txt. Itens mandatórios para esse exercicio:
 *  a) Os registros devem ser salvos dentro de um array() incremental no arquivo
 *  registros.txt
 *  b) O formulário deverá validar os campos email e telefone aceitando somente os
 *  formatos aceitáveis
 *  c) Se possivel nao salvar email ou logins que ja foram registrados anteriormente
 *  d) O campo senha deverá ser salvo encriptado.
 */
?>

<html>
    <head>
        <title>Desafio CI&T - Exercício 4</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <script type='text/javascript'>

            function sendInformation() {
                if (validInformation()) {
                    $.ajax({
                        type: 'POST',
                        url: 'exercicio4_x.php',
                        data: $('form').serialize(),
                        
                        success: function(result) {
                            alert(result);
                        },
                        error: function() {
                            alert('Problema ao inserir registro');
                        }
                    });
                }
            }

            //b
            function validInformation() {
                var check = true;
                
                if ( !$("#firstName").val() ) {
                    alert('Preencher Nome!');
                    return false;
                }

                if ( !$("#lastName").val() ) {
                    alert('Preencher Sobrenome!');
                    return false;
                }

                if ( !$("#userEmail").val() ) {
                    alert('Preencher Email!');
                    return false;
                }

                if ( !$("#userPhone").val() ) {
                    alert('Preencher Telefone!');
                    return false;
                }

                if ( !$("#userLogin").val() ) {
                    alert('Preencher Login!');
                    return false;
                }

                if ( !$("#userPass").val() ) {
                    alert('Preencher Senha!');
                    return false;
                }

                return check;
            }
        </script>

    </head>
    <body>
        <form name='userForm' id='userForm'>
            <div class='field'>
                <label for='firstName'>Nome:</label>
                <input type='text' name='firstName' id='firstName' value='' />
            </div>

            <div class='field'>
                <label for='lastName'>Sobrenome:</label>
                <input type='text' name='lastName' id='lastName' value='' />
            </div>

            <div class='field'>
                <label for='userEmail'>Email:</label>
                <input type='email' name='userEmail' id='userEmail' value='' />
            </div>

            <div class='field'>
                <label for='userPhone'>Telefone:</label>
                <input type='number' name='userPhone' id='userPhone' value='' />
            </div>

            <div class='field'>
                <label for='userLogin'>Login:</label>
                <input type='text' name='userLogin' id='userLogin' value='' />
            </div>

            <div class='field'>
                <label for='userPass'>Senha:</label>
                <input type='password' name='userPass' id='userPass' value='' />
            </div>

            <div class='button'>
                <button name="submit" onClick="sendInformation();">Cadastrar!</button>
            </div>
        </form>
    </body>
</html>