<?php
/**
 * Crie um script PHP que mostra o nome do país e sua capital usando uma
 * array chamada $location. Ordene a Lista pelo nome da capital e adicione
 * pelo menos 5 entradas no array.
 * 
 * Exemplo do que deve ser mostrado de saída:
 *   A capital do Brasil e Brasília
 *   A capital dos EUA e Washington
 *   A capital da Espanha e Madrid
 */

 echo "<b>Exercício 1: </b><br /><br />";
 echo "Crie um script PHP que mostra o nome do país e sua capital usando uma
 array chamada <b>'location'</b>. Ordene a Lista pelo <b>nome da capital</b> e adicione
 pelo menos 5 entradas no array. <br /><br />";
 
 $location = array();

 //Populando o array. Como não foi definido a forma, vou popular manualmente.
 $location = array(
    'Brasilia'      => 'Brasil',
    'Washington'    => 'Estados Unidos',
    'Madrid'        => 'Espanha',
    'Paris'         => 'França',
    'Lisboa'        => 'Portugal',
    'Santiago'      => 'Chile',
    'Buenos Aires'  => 'Argentina'
 );

 //Ordenando o array com base na capital
 ksort($location);

 //Exibindo o resultado
 foreach ($location as $capital => $country) {
     echo 'A capital do <b>' . $country . '</b> é <b>' . $capital . '</b><br />';
 }
 